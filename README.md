# Final_Project FS-2 group1 - Hotel GriLive Project

[![Build Status](https://travis-ci.org/Romik9/Final_Project.svg?branch=master)](https://travis-ci.org/Romik9/Final_Project)
[![Coverage Status](https://coveralls.io/repos/github/Romik9/Final_Project/badge.svg?branch=master)](https://coveralls.io/github/Romik9/Final_Project?branch=master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=final-project&metric=alert_status)](https://sonarcloud.io/dashboard?id=final-project)

### Description
This is a mobile-first application, designed to enable internal communications between the hotel staff.

This project was developed on the basis of backend technologies: Spring Boot, Spring Security, Spring Data, Spring Websockets, Amazon SDK, AWS S3, AWS EC2, AWS RDS, H2, JUnit, Mockito.
Frontend technologies: React, React router, Redux, also NodeJS
The development was carried with continuous integration and was built using the following technologies: Coveralls, Sonarcloud, ESLint, Stylelint, Travis CI Spring. 
During the course of the project, I've learned how to create a flexible RESTful web-app and produce maintainable code.
Teamwork experience was another significant advantage.




### Mentor: 
   [Andrew Koziulia](https://github.com/zozich/ "Andrey Koziulia")

### Developers:
   [Artem Tymchuk](https://github.com/ArtemTymchuk/ "Artem Tymchuk")
   
   [Dmytro Semenchenko](https://github.com/DimitrySemenchenko/ "Dmytro Semenchenko")
   
   [Dmytro Vlasenko](https://github.com/VlasD/ "Dmytro Vlasenko")
   
   [Myroslav Polischuk](https://github.com/Myroslav-Polishchuk/ "Myroslav Polischuk")
   
   [Roman Koreshnyak](https://github.com/Romik9/ "Roman Koreshnyak")
   
   [Rostyslav Barmakov](https://github.com/warfawl/ "Rostyslav Barmakov")
   

   
